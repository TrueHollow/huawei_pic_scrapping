const cheerio = require('cheerio');
const Net = require('./Net');
const io = require('./IO');
const logger = require('../logger')('service/Parser.js');

function getTransCode(originParam) {
  let param = originParam;
  if (param != null && param !== '') {
    if (param.indexOf('&') > -1) {
      param = param.replace(/&/g, '|');
    }
    if (param.indexOf('+') > -1) {
      param = param.replace(/\+/g, '$');
    }
    if (param.indexOf('(') > -1) {
      param = param.replace(/\(/g, '[');
    }
    if (param.indexOf(')') > -1) {
      param = param.replace(/\)/g, ']');
    }
  }
  return param;
}

const LINKS_ROOT = 'https://support.huawei.com/onlinetoolweb/Network_imagelib/';
const ROOT_URL =
  'https://support.huawei.com/onlinetoolweb/Network_imagelib/index?domain=0&lang=zh';
const DETAILS_ENDPOINT =
  'https://support.huawei.com/onlinetoolweb/Network_imagelib/getImageBySeries';

class Parser {
  constructor(config) {
    this.config = config;
  }

  async getRootLinks() {
    logger.info('Get html root and parse it');
    const html = await Net.GetHtmlPage(ROOT_URL);
    const $ = cheerio.load(html);
    const rawLinks = $('.tobrowser');
    const result = [];
    rawLinks.each((i, aTag) => {
      const type = $(aTag).text();
      const family = $(aTag)
        .parent('li')
        .parent('ul')
        .parent('div')
        .parent('.box')
        .find('.name')
        .text();
      const url = `${LINKS_ROOT}getImagePartList?product_family=${getTransCode(
        family
      )}&product_type=${getTransCode(type)}&domain=0&lang=zh`;
      result.push(encodeURI(url));
    });
    return result;
  }

  async getMetaSubLinks(rootLink) {
    logger.info('Get meta sub links');
    const html = await Net.GetHtmlPage(rootLink);
    const $ = cheerio.load(html);
    const rawLinks = $('.auto-serise');
    const productfamily = $('#product_family').text();
    const producttype = $('#product_type').text();

    const serise = [];
    rawLinks.each((i, aTag) => {
      const con = $(aTag);
      const name = con.text();
      const item = {};
      if (con.hasClass('subserise')) {
        item.subserise = name;
        item.serise = con
          .parent()
          .parent()
          .parent()
          .find('.serise')
          .text();
      } else if (con.hasClass('serise')) {
        item.serise = name;
      } else {
        const classList = con.attr('class').split(/\s+/);
        logger.warn(
          'Unexpected classes on tag (%s)',
          JSON.stringify(classList)
        );
      }
      serise.push(item);
    });
    return {
      serise,
      productfamily,
      producttype,
    };
  }

  async processMetaSubLinks(subMetaLinks) {
    const { serise, productfamily, producttype } = subMetaLinks;
    // eslint-disable-next-line no-restricted-syntax
    for (const s of serise) {
      const sendObject = {
        product_family: productfamily,
        product_type: producttype,
        product_series: s.serise,
        domain: '0',
        lang: 'zh',
      };
      if (s.subserise) {
        sendObject.product_subseries = s.subserise;
      }
      logger.info(`Get details for (${JSON.stringify(sendObject)})`);
      // eslint-disable-next-line no-await-in-loop
      const jsonDetails = await Net.GetJsonDetails(
        DETAILS_ENDPOINT,
        sendObject
      );

      if (jsonDetails.length === 0) {
        logger.warn(`No details for (${JSON.stringify(sendObject)})`);
      } else {
        logger.info(`Found ${jsonDetails.length} details.`);
        const { ZipRequestLimit } = this.config;
        do {
          const iteration = jsonDetails.splice(0, ZipRequestLimit);
          const promises = iteration.map(async json => {
            const {
              downloadPic,
              productName,
              productFamily,
              productType,
            } = json; // "downloadVss"
            if (!downloadPic) {
              return logger.warn('No downloadPic for %s', json);
            }
            if (!productName) {
              return logger.warn('No productName for %s', json);
            }
            if (await io.FileExist(productFamily, productType, downloadPic)) {
              return logger.info(
                'File (%s) already exist. Skipping.',
                downloadPic
              );
            }
            logger.info('Downloading %s', downloadPic);
            // https://support.huawei.com/onlinetoolweb/Network_imagelib/downloadZip?productName=S2700-18TP-EI-AC&imgType=pic&domain=0&lang=zh
            const url = encodeURI(
              `https://support.huawei.com/onlinetoolweb/Network_imagelib/downloadZip?productName=${productName}&imgType=pic&domain=0&lang=zh`
            );
            const buffer = await Net.GetFile(url);
            logger.info('Saving %s.', downloadPic);
            return io.SaveFile(productFamily, productType, downloadPic, buffer);
          });
          // eslint-disable-next-line no-await-in-loop
          await Promise.all(promises);
        } while (jsonDetails.length);
      }
    }
  }

  async run() {
    const rootLinks = await this.getRootLinks();

    // eslint-disable-next-line no-restricted-syntax
    for (const rootLink of rootLinks) {
      // eslint-disable-next-line no-await-in-loop
      const subMetaLinks = await this.getMetaSubLinks(rootLink);
      // eslint-disable-next-line no-await-in-loop
      await this.processMetaSubLinks(subMetaLinks);
    }
  }
}

module.exports = Parser;
