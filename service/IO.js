const path = require('path');
const fs = require('fs');
const logger = require('../logger')('service/IO.js');

const OUTPUT_DIRECTORY = path.resolve(__dirname, '../output');

const fileExist = async fullPath => {
  return new Promise((resolve, reject) => {
    fs.access(fullPath, fs.constants.F_OK, err => {
      if (err) {
        return reject(err);
      }
      return resolve();
    });
  });
};

const createFolder = async fullPath => {
  return new Promise((resolve, reject) => {
    logger.debug('Checking output directory.');
    fs.mkdir(fullPath, { recursive: true }, err => {
      if (err) {
        return reject(err);
      }
      return resolve();
    });
  });
};

class IO {
  static get OutputDirectory() {
    return OUTPUT_DIRECTORY;
  }

  static async Prepare() {
    const outputFullpath = `${OUTPUT_DIRECTORY}`;
    return createFolder(outputFullpath);
  }

  static async SaveFile(folder, subFolder, fileName, buffer) {
    const folderFullpath = path.resolve(OUTPUT_DIRECTORY, folder, subFolder);
    await createFolder(folderFullpath);
    return new Promise((resolve, reject) => {
      const fullPath = path.resolve(folderFullpath, fileName);
      fs.writeFile(fullPath, buffer, err => {
        if (err) {
          return reject(err);
        }
        return resolve();
      });
    });
  }

  static async FileExist(folder, subFolder, fileName) {
    const fullPath = path.resolve(
      OUTPUT_DIRECTORY,
      folder,
      subFolder,
      fileName
    );
    try {
      await fileExist(fullPath);
      return true;
    } catch (e) {
      return false;
    }
  }
}

module.exports = IO;
