const request = require('request');
const logger = require('../logger')('service/Net.js');

const HEADERS = {
  'User-Agent':
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36',
};

const HTTP_OK = 200;

const delay = async (timeout = 5000) => {
  return new Promise(resolve => {
    setTimeout(resolve, timeout);
  });
};

const GetRequest = async (url, encoding = undefined) => {
  logger.debug(`Performing request to (${url})...`);
  return new Promise((resolve, reject) => {
    request(
      {
        url,
        method: 'GET',
        headers: HEADERS,
        encoding,
        timeout: 20000,
      },
      async (err, incomingMessage, html) => {
        if (err) {
          return reject(err);
        }
        if (incomingMessage.statusCode !== HTTP_OK) {
          return reject(
            new Error(`Response code: ${incomingMessage.statusCode}`)
          );
        }
        logger.debug(`...request (${url}) is finished`);
        return resolve(html);
      }
    );
  });
};

const PostRequest = async (url, obj) => {
  logger.debug(`Performing request to (${url})...`);
  return new Promise((resolve, reject) => {
    request(
      {
        url,
        method: 'POST',
        headers: {
          'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
          ...HEADERS,
        },
        form: obj,
        json: true,
        gzip: true,
        timeout: 20000,
      },
      async (err, incomingMessage, json) => {
        if (err) {
          return reject(err);
        }
        if (incomingMessage.statusCode !== HTTP_OK) {
          return reject(
            new Error(`Response code: ${incomingMessage.statusCode}`)
          );
        }
        logger.debug(`...request (${url}) is finished`);
        return resolve(json);
      }
    );
  });
};

class Net {
  static async GetJsonDetails(url, obj) {
    let json;
    do {
      try {
        // eslint-disable-next-line no-await-in-loop
        json = await PostRequest(url, obj);
      } catch (e) {
        logger.error('Network error: %s', e);
        // eslint-disable-next-line no-await-in-loop
        await delay();
        logger.debug('Restart request');
      }
    } while (!json);
    return json;
  }

  static async GetHtmlPage(url) {
    let html;
    do {
      try {
        // eslint-disable-next-line no-await-in-loop
        html = await GetRequest(url);
      } catch (e) {
        logger.error('Network error: %s', e);
        // eslint-disable-next-line no-await-in-loop
        await delay();
        logger.debug('Restart request');
      }
    } while (!html);
    return html;
  }

  static async GetFile(url) {
    let buffer;
    do {
      try {
        // eslint-disable-next-line no-await-in-loop
        buffer = await GetRequest(url, null);
      } catch (e) {
        logger.error('Network error: %s', e);
        // eslint-disable-next-line no-await-in-loop
        await delay();
        logger.debug('Restart request');
      }
    } while (!buffer);
    return buffer;
  }
}

module.exports = Net;
