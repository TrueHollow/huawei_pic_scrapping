const config = require('./config');
const logger = require('./logger')('index.js');
const io = require('./service/IO');
const Parser = require('./service/Parser');

logger.info('Script is started');

const main = async () => {
  await io.Prepare();
  const parser = new Parser(config.Parser);
  return parser.run();
};

main().then(() => {
  logger.info('Script is finished.');
});
